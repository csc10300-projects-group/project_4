/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 3-4 hrs
 */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
using namespace std;
#include<iostream>
#include <set>
using std::set;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

const char* space = " ";
const char* tab = "\t";
const char* newline = "\n";

//Function to determine if a char is whitespace
bool isWs(char c)
{
	if(c == *space || c == *tab || c == *newline)
		return true;

	return false;
}

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	char input;							//holds input char
	bool ws = true;					//state: true when 'just read whitespace'
	bool oneLine = true;		//keeps track if input is only one line

	set<string> S;					//holds unique words
	string word = "";				//string to build every word

	int bytes = 0;					//# of bytes in input
	int lines = 0;					//# of new lines in input
	int words = 0;					//# of overall words in input
	int uwords = 0;					//# of unique words in input
	int len = 0;						//length of the current line being read
	int maxline = 0;				//length of the longest line read so far

	while(fread(&input,1,1,stdin))
	{
		bytes = bytes + sizeof(input);		//adds bytes

		if(input == *newline)
		{
			oneLine = false;
			lines++;								//increment lines
			if(len > maxline)				//rewrite maxline if needed
				maxline = len;
			len = 0;								//reset current line length to 0
		}

		if(input == *tab)
			len += 8 - len%8;				//add to length of line for tab
		else
			len++;									//increment length for general case

		if(ws && !isWs(input))		//condition for state change
		{
			ws = false;
			words++;								//found a new word; increment
			word = word + input;		//begin building string for new word
		}

		if(!ws)
		{
			if(isWs(input))					//condition for state change
			{
				ws = true;						//change state
				S.insert(word);				//add word to set of words
				word = "";						//prepare to read a new word
			}
			else
				word = word + input;	//continue building word
		}

	}

	S.insert(word);							//add final word
	uwords = S.size();					//size of set is # of unique words


	if(oneLine)
		maxline = len;						//if only one line, maxline is len

	//Print statements
	if(linesonly == 1)
	{
		printf("%i", lines);
		printf(" ");
	}

	if(wordsonly == 1)
	{
		printf("%i", words);
		printf(" ");
	}

	if(charonly == 1)
	{
		printf("%i", bytes);
		printf(" ");
	}

	if(longonly == 1)
	{
		printf("%i", maxline);
		printf(" ");
	}

	if(uwordsonly == 1)
	{
		printf("%i", uwords);
		printf(" ");
	}

	if(linesonly + wordsonly + charonly + longonly + uwordsonly == 0)
	{
		printf("%i", lines);
		printf(" ");
		printf("%i", words);
		printf(" ");
		printf("%i", bytes);
		printf(" ");
	}

	printf("\n");


	return 0;
}
