#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */
int ignorecase = 0;
int compare(string str1, string str2) {
	if (ignorecase == 1) {
		return strcasecmp(str1.c_str(),str2.c_str());
	} else {
		return str1.compare(str2);
	};
};

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	struct DLinkedList {
		struct Node {
			int count = 1;
			string data;
			Node* prev = NULL;
			Node* next = NULL;

			Node(string _data) {
				data = _data;
			};

			Node(string _data, Node* _next) {
				data = _data;
				next = _next;
			};

			Node(string _data, Node* _prev, Node* _next) {
				data = _data;
				prev = _prev;
				next = _next;
			};
		};

		Node* head = NULL;
		Node* tail = NULL;
		int size = 0;

		void addNode(string input) {
			if (head == NULL) { // Empty list, adding initial node
				head = tail = new Node(input);
			} else { // Non-empty list
				if (compare(input, head->data) <= 0) { // New entry should either equals head, or is before head
					if (compare(input, head->data) == 0) { // New entry equals head
						head->count++;
					} else { // New entry is before head
						head->prev = new Node(input, head);
						head = head->prev;
					};
				} else { // New entry comes somepoint after head
					Node* currNode = head;
					Node* nextNode = currNode->next;

					if (nextNode == NULL) { // Only one entry in the list, adding as 2nd
						tail = currNode->next = new Node(input);
						tail->prev = head;
					} else { // More than two entries in the list
						while (compare(input, currNode->data) > 0 && nextNode != NULL) { // Find spot where newNode should be after currNode, and a nextNode exists
							if (compare(input, nextNode->data) < 0) { // If newNode should be before nextNode, insert node
								currNode->next = currNode->next->prev = new Node(input, currNode, nextNode);
								break;
							} else if (compare(input, nextNode->data) == 0) { // If newNode is equal to nextNode, increment counter
								nextNode->count++;
								break;
							};

							// Continue iterations
							currNode = nextNode;
							nextNode = nextNode->next;
						};

						if (nextNode == NULL) { // Could not find spot for newNode to be inserted, so inserting at the end
							tail = currNode->next = new Node(input);
							tail->prev = currNode;
						};

					};
				};
			};

			size++;
		};

		void printList() {
			Node* currNode;
			descending == 0 ? currNode = head : currNode = tail;

			while (currNode != NULL && size != 0) {
				if (unique == 0) {
					for (int i = 0; i < currNode->count; i++) {
						cout << currNode->data << '\n';
					};
				} else {
					cout << currNode->data << '\n';
				};

				descending == 0 ? currNode = currNode->next : currNode = currNode->prev;
			};
		};
	};

	DLinkedList* list = new DLinkedList();

	for (string line; getline(cin, line);) {
		list->addNode(line);
	};

	list->printList();

	return 0;
}
