/*Resources:
https://www.geeksforgeeks.org/uniq-command-in-linux-with-examples/
https://shapeshed.com/unix-uniq/
https://stackoverflow.com/questions/7769715/c-and-printf-strange-character-output

Purpose of code:
1.show count before each line -c
2.only show duplicated lines -d
3.only show unique lines -u
*/
#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */


string word,temp="";
size_t count = 0;

if(dupsonly && uniqonly)
	return 0;
if(dupsonly &&uniqonly &&showcount)
	return 0;

while(getline(cin,word))
{
	if(temp != word)//new word
	{
		if((!uniqonly && !dupsonly) ||(dupsonly &&count>1) || (uniqonly&& count ==1))
		{
			if(showcount)
			{
				printf("%7lu ",count);
			}
			cout<<temp<<"\n";
		}
		count=1;
	}
	else //if the word is the same, increase count
		{
			count++;
		}
		temp=word;
}
if((!uniqonly&&!dupsonly) || (dupsonly&&count>1)||(uniqonly &&count==1))
{
	if(showcount)
	{
		printf("%7lu ",count);
	}
	cout<<temp<<"\n";
}
	return 0;
}
