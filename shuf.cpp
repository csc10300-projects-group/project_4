#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;
using std::endl;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */
	srand(time(0));
	if(userange){
		unsigned int rangeValue = rhigh -rlow + 1;
		vector <unsigned int> value;
		vector <unsigned int> value2(rangeValue);
			for (unsigned int i = rlow; i < rhigh + 1; i++){
				value.push_back(i);
			}
			for (size_t j = 0; j < value2.size(); j++) {
				unsigned int index = rand()% value.size();
				value2[j] = value[index];
				value.erase(value.begin() + index);
			}
			for (size_t i = 0; i < value2.size(); i++){
				cout << value2[i] << endl;
			}
		}
	else{
		vector <string> stringVec;
		string words;
		if (echo){
			while (optind < argc) {
				stringVec.push_back(argv[optind++]);
			}
		}
		else {
			while (getline (cin,words)){
				stringVec.push_back(words);
			}
		}
		vector <string> stringVec2 (stringVec);
			for (size_t k = 0; k <stringVec2.size(); k++){
			unsigned int index = rand()% stringVec.size();
			stringVec2[k] = stringVec[index];
			stringVec.erase(stringVec.begin() + index);
		}
		size_t maxValue = 0;
		if (count < stringVec2.size() && count != -1){
			maxValue = count;
		}
		else{
			maxValue = stringVec2.size();
		}
		for (size_t l = 0; l < maxValue; l++){
			cout <<stringVec2[l] << endl;
		}
	}
	return 0;
}
