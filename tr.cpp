/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: Extremely tedious to figure out escape character behaviour... otherwise maybe 3-4 hrs to actually write the logic
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

const char* nl = "\n";
const char* tab = "\t";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */

	//removes the backslash for unrecognized sequences
	vector<size_t> remove;
	for(size_t i = 0; i < s.length() - 1; i++)
		if(s[i] == '\\')
			if(s[i+1] != 'n' && s[i+1] != 't' && s[i+1] != '\\')
				remove.push_back(i);

	for(int i = remove.size() - 1; i >= 0; i--)
			s.erase(remove[i],1);

	//returns if the string is only 1 char to avoid crash
	if(s.length() < 2)
		return;

	//converts recognized sequences into corresponding chars
	string converted = "";
			for(size_t i = 0; i < s.length(); i++)
				if(s[i] != '\\')
					converted += s[i];
				else{
					if(s[i+1] == 'n')
						converted += *nl;
					if(s[i+1] == 't')
						converted += *tab;
					if(s[i+1] == 'n' || s[i+1] == 't')
						i++;
				}

	s = converted;
}

//converts "a-e" into "abcde" etc.
void range(string& s) {
	string expanded = "";
	vector<size_t> dash;
	for(size_t i = 1; i < s.length() - 1; i++)
		if(s[i] == '-')
			dash.push_back(i);

	if(dash.size() == 0)
		return;

	for(size_t i = 0; i < dash.size(); i++)
		for(char c = s[dash[i]-1]; c <= s[dash[i]+1]; c++)
			expanded += c;

	s = expanded;
}

//fills vector with chars from given string
void fill(vector<char>& v, string s) {
	for(size_t i = 0; i < s.length(); i++)
		v.push_back(s[i]);
}

//makes second set match size of first set
void resize(vector<char>& a, vector<char>& b) {
	while(a.size() > b.size())
		b.push_back(b[b.size()-1]);
	while(b.size() > a.size())
		b.pop_back();
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	vector<char> SET1;
	vector<char> SET2;

	//process set 1
	escape(s1);
	range(s1);
	fill(SET1, s1);

	//process set 2 if not deleting
	if(del != 1)
	{
		escape(s2);
		range(s2);
		fill(SET2, s2);
		resize(SET1, SET2);
	}

	char input;
	string output = "";
	while(fread(&input,1,1,stdin))
	{
		//4 cases shown below for combinations between -c and -d:

		if(comp != 1 && del != 1)
		{
			char add;
			bool swap = false;
			for(size_t i = 0; i < SET1.size(); i++)
				if(input == SET1[i])
				{
					add = SET2[i];
					swap = true;
				}

			if(!swap)
				output += input;
			else
				output += add;
		}

		if(comp == 1 && del != 1)
		{
			char add;
			bool swap = false;
			for(size_t i = 0; i < SET1.size(); i++)
				if(input != SET1[i])
				{
					add = SET2[SET2.size()-1];
					swap = true;
				}

			if(!swap)
				output += input;
			else
				output += add;
		}

		if(comp != 1 && del == 1)
		{
			bool rm = false;
			for(size_t i = 0; i < SET1.size(); i++)
				if(input == SET1[i])
					rm = true;

			if(!rm)
				output += input;
		}

		if(comp == 1 && del == 1)
		{
			bool rm = true;
			for(size_t i = 0; i < SET1.size(); i++)
				if(input == SET1[i])
					rm = false;

			if(!rm)
				output += input;
		}
	}

	cout << output;

	return 0;
}
